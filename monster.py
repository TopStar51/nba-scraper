from bs4 import BeautifulSoup
from unidecode import unidecode
from selenium import webdriver
from random import randint
import requests
import time
import xlsxwriter
import re

site_name = "MONSTER"
filename = 'monster.xlsx'

username = "Jmure007"
password = "MSRLMRNMAF"

base_url = "https://basketballmonster.com/dailyprojections.aspx"

user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(' ')
    if len(splits) == 3:
        if len(splits[0]) == 2 and splits[0][1] == '.' and len(splits[1]) == 2 and splits[1][1] == '.':
            result = splits[0][0] + splits[1][0] + " " + splits[2]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1]
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result

def write_data(worksheet, row, player_name, daily_projection, price1='none', price2='none'):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, daily_projection)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def monster_odds():
    print('---Start Basketballmonster---')
    workbook = xlsxwriter.Workbook(filename)

    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    # Add three points tab
    three_points_sheet = workbook.add_worksheet('3-pointers')
    # Add PRA tab
    workbook.add_worksheet('PRA')
    # Placeholder sheet
    workbook.add_worksheet('Dbl-Dbl')
    
    point_row = 0
    rebound_row = 0
    assist_row = 0
    three_points_row = 0

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument('--user-agent={}'.format(user_agent))
    options.add_argument('log-level=2')

    driver = webdriver.Chrome(options=options)

    driver.get('https://basketballmonster.com/login.aspx')
    time.sleep(5)

    try:
        username_elem = driver.find_element_by_id('UsernameTB')
        username_elem.send_keys(username)
        time.sleep(randint(2, 4))

        pass_elem = driver.find_element_by_id('PasswordTB')
        pass_elem.send_keys(password)
        time.sleep(randint(2, 4))

        btn_login = driver.find_element_by_id('LoginButton')
        btn_login.click()
        time.sleep(randint(3, 5))

        driver.get(base_url)
        time.sleep(randint(5, 8))
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        try:
            datatable = soup.find('div', {'class': 'results-table'}).find('tbody')
            trs = datatable.find_all('tr')
            for tr in trs:
                try:
                    tds = tr.find_all('td')
                    write_data(points_sheet, point_row, remove_spec(tds[6].get_text()), tds[16].get_text())
                    point_row += 1
                    write_data(rebounds_sheet, rebound_row, remove_spec(tds[6].get_text()), tds[18].get_text())
                    rebound_row += 1
                    write_data(assists_sheet, assist_row, remove_spec(tds[6].get_text()), tds[19].get_text())
                    assist_row += 1
                    write_data(three_points_sheet, three_points_row, remove_spec(tds[6].get_text()), tds[17].get_text())
                    three_points_row += 1
                except:
                    continue
        except:
            print("basketballmonster: There is no data")
    except:
        pass
    
    workbook.close()
    driver.close()
    driver.quit()
    print('---End Basketballmonster---')
    
if __name__ == "__main__":
    monster_odds()