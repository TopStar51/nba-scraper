from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import randint
from unidecode import unidecode
from datetime import datetime
import time
import xlsxwriter
import sys
import re
import json

with open('config.json') as cf:
    config = json.load(cf)

user_agent = config['user_agent']
timeout = config['timeout']

site_name = "POINTSBET"
filename = 'pointsbet.xlsx'
odd_names = ["player points over/under", "player rebounds over/under","player assists over/under", "player pts + rebs + asts over/under"]

def write_data(worksheet, row, player_name, label, price1, price2):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, label)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(' ')
    if len(splits) == 3:
        if len(splits[0]) == 2 and splits[0][1] == '.' and len(splits[1]) == 2 and splits[1][1] == '.':
            result = splits[0][0] + splits[1][0] + " " + splits[2]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1]
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result
    
def pointsbet_odds():
    print('---Start Pointsbet---')
    # cur_day = datetime.now().strftime("%A")

    workbook = xlsxwriter.Workbook(filename)

    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    # Add 3-pointers tab(Now, this market does not exist.)
    workbook.add_worksheet('3-pointers')
    pra_sheet = workbook.add_worksheet('PRA')
    # Placeholder sheet
    workbook.add_worksheet('Dbl-Dbl')

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument('--user-agent={}'.format(user_agent))
    options.add_argument('log-level=2')

    driver = webdriver.Chrome(options=options)

    driver.get('https://nj.pointsbet.com/basketball/NBA')
    #time.sleep(randint(10, 15))
    try:
        WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//a[contains(@href, "sports/basketball/NBA")]')))
    except TimeoutException:
        print('Pointsbet: There is no nba market')
        driver.close()
        driver.quit()
        workbook.close()
        return
    
    nba_games = driver.find_elements_by_xpath('//a[contains(@href, "sports/basketball/NBA")]')
    if len(nba_games) == 0:
        print('Pointsbet: There is no nba market')
        driver.close()
        driver.quit()
        workbook.close()
        return

    point_row = 0
    rebound_row = 0
    assist_row = 0
    pra_row = 0

    nba_links = []
    for nba_game in nba_games:
        nba_links.append(nba_game.get_attribute('href'))
    
    for nba_link in nba_links:
        print(nba_link)
        driver.get(nba_link)
        time.sleep(randint(3, 5))
        try:
            fixed_odds_tab = driver.find_element_by_css_selector('div.ffruk4d')
            markets = fixed_odds_tab.find_elements_by_css_selector('button.f1pa2e9k.fxoyt2i')
            # markets = driver.find_element_by_css_selector('div.ffruk4d').find_elements_by_css_selector('button.fnoejpg.fdhcmmx')
        except TimeoutException:
            print("Pinnacle: Timed out waiting for page to load. Please try again.")
            workbook.close()
            driver.close()
            driver.quit()
            print("Pointsbet: There is no available markets in this match")
            continue
        
        try:
            for market in markets:
                market_text = market.text.lower()
                if "featured markets" in market_text:
                    driver.execute_script("arguments[0].click();", market)
                    continue
                
                if "points" in market_text or "assists" in market_text or "rebounds" in market_text or "multi stat" in market_text:
                    driver.execute_script("arguments[0].click();", market)
                    time.sleep(2)
                    parent_div = market.find_element_by_xpath('..')
                    children_div = parent_div.find_elements_by_css_selector('div.f1o33gnc.flp1t0e')
                    for child_div in children_div:
                        odd_name = child_div.find_element_by_css_selector('h3.f10wqgzd.fssprbn').text.strip().lower()
                        
                        if odd_name in odd_names:
                            try:
                                see_all_btn = child_div.find_element_by_css_selector('button.fq7xcxd')
                                driver.execute_script("arguments[0].click();", see_all_btn)
                                time.sleep(1)
                            except:
                                pass
                            try:
                                outcomes = child_div.find_elements_by_css_selector('div.fukggw2')
                                if len(outcomes) == 0:
                                    outcomes = child_div.find_elements_by_css_selector('button.f10krlro.fxkhq3r.f14nmd6v')
                            except:
                                pass
                            
                            odd_even_flag = 1
                            for outcome in outcomes:
                                if (odd_even_flag % 2) == 1:
                                    try:
                                        outcome_name = outcome.find_element_by_css_selector('div.f2dhou8.f1d2xob0').text
                                    except:
                                        outcome_name = outcome.find_element_by_css_selector('span.fsu5r7i').text
                                    
                                    last_idx = outcome_name.find('Over')
                                    label_start_idx = last_idx + 5
                                    if last_idx == -1:
                                        continue
                                    player_name = remove_spec(outcome_name[0:last_idx].strip())

                                    label = outcome_name[label_start_idx:].strip()
                                    price1 = outcome.find_element_by_css_selector('span.f1368x66').text.strip()
                                else:
                                    price2 = outcome.find_element_by_css_selector('span.f1368x66').text.strip()
                                    if "points" in market_text:
                                        write_data(points_sheet, point_row, player_name, label, price1, price2)
                                        point_row += 1
                                    elif "assists" in market_text:
                                        write_data(assists_sheet, assist_row, player_name, label, price1, price2)
                                        assist_row += 1   
                                    elif "rebounds" in market_text:
                                        write_data(rebounds_sheet, rebound_row, player_name, label, price1, price2)
                                        rebound_row += 1 
                                    else:
                                        write_data(pra_sheet, pra_row, player_name, label, price1, price2)
                                        pra_row += 1
                                odd_even_flag += 1
                    driver.execute_script("arguments[0].click();", market)   
                    time.sleep(0.5)
        except:
            continue
            
    workbook.close()
    driver.close()
    driver.quit()
    print('---End Pointsbet---')

if __name__ == "__main__":
    pointsbet_odds()