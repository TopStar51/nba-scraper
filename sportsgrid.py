from time import mktime, time
from datetime import datetime
from unidecode import unidecode
import requests
import json
import xlsxwriter
import re

site_name = "SPORTSGRID"
filename = 'sportsgrid.xlsx'

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(' ')
    if len(splits) == 3:
        if len(splits[0]) == 2 and splits[0][1] == '.' and len(splits[1]) == 2 and splits[1][1] == '.':
            result = splits[0][0] + splits[1][0] + " " + splits[2]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1]
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result

def write_data(worksheet, row, player_name, daily_projection, price1='none', price2='none'):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, daily_projection)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def sportsgrid_odds():
    print('---Start Sportsgrid---')
    workbook = xlsxwriter.Workbook(filename)
    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    three_points_sheet = workbook.add_worksheet('3-pointers')
    # Do not exist for now.
    workbook.add_worksheet('PRA')
    # Placeholder sheet
    workbook.add_worksheet('Dbl-Dbl')
    
    t = mktime(datetime.now().timetuple())
    page = requests.get('https://www.sportsgrid.com/wp-admin/admin-ajax.php?action=getPropsData&t={}&sportsType=nba&nowp'.format(t))
    if page.status_code == 200:
        try:
            json_data = json.loads(page.text)

            points_row = 0
            assists_row = 0
            rebounds_row = 0
            three_point_row = 0

            for player in json_data['data']:
                player_name = remove_spec(player['PlayerName'])
                if 'Points' in player['PropType']:
                    write_data(points_sheet, points_row, player_name, player['Projection'])
                    points_row += 1
                elif 'Assists' in player['PropType']:
                    write_data(assists_sheet, assists_row, player_name, player['Projection'])
                    assists_row += 1
                elif 'Rebounds' in player['PropType']:
                    write_data(rebounds_sheet, rebounds_row, player_name, player['Projection'])
                    rebounds_row += 1
                elif '3 Point FG Made' in player['PropType']:
                    write_data(three_points_sheet, three_point_row, player_name, player['Projection'])
                    three_point_row += 1
                else:
                    continue
        except:
            pass
    else:
        print('Could not fetch site content')
    workbook.close()
    print('---End Sportsgrid---')

if __name__ == "__main__":
    sportsgrid_odds()