from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import randint
from unidecode import unidecode
from datetime import datetime
import re
import time
import xlsxwriter
import pytz
import json

with open('config.json') as cf:
    config = json.load(cf)

user_agent = config['user_agent']
timeout = config['timeout']

site_name = "FANDUEL"
filename = 'fanduel.xlsx'

tz = pytz.timezone('America/New_York')

def write_data(worksheet, row, player_name, label, price1, price2):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, label)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(' ')
    if len(splits) == 3:
        if len(splits[0]) == 2 and splits[0][1] == '.' and len(splits[1]) == 2 and splits[1][1] == '.':
            result = splits[0][0] + splits[1][0] + " " + splits[2]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1]
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result
    
def fanduel_odds():
    print('---Start Fanduel---')
    cur_time = datetime.now(tz).strftime("%m/%d/%Y")

    workbook = xlsxwriter.Workbook(filename)

    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    three_points_sheet = workbook.add_worksheet('3-pointers')
    pra_sheet = workbook.add_worksheet('PRA')
    # Placeholder sheet
    dbl_sheet = workbook.add_worksheet('Dbl-Dbl')
    
    idx = 0
    points_row = 0
    assists_row = 0
    rebounds_row = 0
    three_points_row = 0
    pra_row = 0

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument('--user-agent={}'.format(user_agent))
    options.add_argument('log-level=2')

    driver = webdriver.Chrome(options=options)

    driver.get('https://sportsbook.fanduel.com/sports/navigation/830.1/8047.1')
    
    while True:
        try:
            marget_groups = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CLASS_NAME, "marketgroups")))
        except TimeoutException:
            print("Fanduel: Timed out waiting for page to load. Please try again.")
            break
        
        events = marget_groups.find_elements_by_class_name('event')
        if idx == len(events):
            break
        try:
            market_date = events[idx].find_element_by_css_selector("div.time").text.split(' ')[0]
        except:
            idx += 1
            continue
        if market_date not in ("Today", cur_time):
            idx += 1
            continue

        try:
            more_market = events[idx].find_element_by_class_name('more-markets')
            driver.execute_script("arguments[0].click();", more_market)
            time.sleep(2)
            try:
                driver.find_element_by_link_text('Player Props').click()
            except:
                driver.back()
                idx += 1
                continue
            time.sleep(1)
            print(driver.current_url)
            event_market_group = driver.find_element_by_class_name('eventmarketgroup')
            event_markets = event_market_group.find_elements_by_class_name('event-market-header')
            first_open_icon = event_markets[0].find_element_by_class_name('igt-icon')
            driver.execute_script("arguments[0].click();", first_open_icon)
            time.sleep(1)
            for event_market in event_markets:
                title = event_market.find_element_by_class_name('market-name-title').get_attribute('innerHTML')
                if "- Points" in title or "- Rebounds" in title or "- Assists" in title or "- Made Threes" in title or "- Pts + Reb + Ast" in title:
                    player_name = remove_spec(title.split(" - ")[0].strip())
                    expand_icon = event_market.find_element_by_class_name('igt-icon')
                    driver.execute_script("arguments[0].click();", expand_icon)
                    time.sleep(0.5)
                    try:
                        expansion_body = driver.find_element_by_class_name('expansion-panel__body')
                    except:
                        continue
                    try:
                        label = expansion_body.find_element_by_class_name('uo-currenthandicap').get_attribute('innerHTML')
                        price1 = expansion_body.find_elements_by_class_name('selectionprice')[0].get_attribute('innerHTML')
                        price2 = expansion_body.find_elements_by_class_name('selectionprice')[1].get_attribute('innerHTML')
                    except:
                        continue
                    if "Points" in title:
                        write_data(points_sheet, points_row, player_name, label, price1, price2)
                        points_row += 1
                    elif "Rebounds" in title:
                        write_data(rebounds_sheet, rebounds_row, player_name, label, price1, price2)
                        rebounds_row += 1
                    elif "Assists" in title:
                        write_data(assists_sheet, assists_row, player_name, label, price1, price2)
                        assists_row += 1
                    elif "Made Threes" in title:
                        write_data(three_points_sheet, three_points_row, player_name, label, price1, price2)
                        three_points_row += 1
                    else:
                        write_data(pra_sheet, pra_row, player_name, label, price1, price2)
                        pra_row += 1
                    driver.execute_script("arguments[0].click();", expand_icon)
                    time.sleep(0.5)
        except:
            break
        driver.back()
        idx += 1

    workbook.close()
    driver.close()
    driver.quit()
    print('---End Fanduel---')

if __name__ == "__main__":
    fanduel_odds()
