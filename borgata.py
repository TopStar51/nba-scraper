from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from random import randint
from unidecode import unidecode
import re
import time
import xlsxwriter
import sys
import json

with open('config.json') as cf:
    config = json.load(cf)

user_agent = config['user_agent']
timeout = config['timeout']

site_name = "BORGATA"
titles = ["Points", "Assists", "Rebounds", "3-pointers", "PRA", "Dbl-Dbl"]
filename = 'borgata.xlsx'

def write_data(worksheet, row, player_name, label, price1, price2):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, label)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result
   
def borgata_odds():
    print('---Start Borgata---')
    workbook = xlsxwriter.Workbook(filename)

    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    three_points_sheet = workbook.add_worksheet('3-pointers')
    pra_sheet = workbook.add_worksheet('PRA')
    dbl_sheet = workbook.add_worksheet('Dbl-Dbl')

    point_row = 0
    rebound_row = 0
    assist_row = 0
    three_points_row = 0
    pra_row = 0
    dbl_row = 0
    
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument('--user-agent={}'.format(user_agent))
    options.add_argument('log-level=2')

    driver = webdriver.Chrome(options=options)

    driver.get('https://sports.borgataonline.com/en/sports/basketball-7/betting/usa-9/nba-6004')
    # time.sleep(randint(10, 13))
    
    try:
        #close_icon = driver.find_element_by_css_selector('span.ui-icon.theme-ex')
        close_icon = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span.ui-icon.theme-ex')))
        driver.execute_script("arguments[0].click();", close_icon)
        time.sleep(5)
    except:
        pass
    
    # parent_main_view = driver.find_element_by_id('main-view')
    try:
        parent_main_view = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.ID, 'main-view')))
    except TimeoutException:
        print("Borgata: Timed out waiting for page to load")
        driver.close()
        driver.quit()
        workbook.close()
        return
    
    try:
        parent_scroll_view = parent_main_view.find_element_by_xpath('..')
        scroll_height = parent_main_view.size['height']
        
        try:
            event_group = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CLASS_NAME, 'event-group')))
        except TimeoutException:
            print("Borgata: Timed out waiting for page to load")
            driver.close()
            driver.quit()
            workbook.close()
            return
            
        events = event_group.find_elements_by_class_name('grid-event-wrapper')
        if len(events) == 0:
            print('Borgata: There is no nba market')
            driver.close()
            driver.quit()
            workbook.close()
            return
        elif len(events) > 4:
            y = 300
            while True:
                driver.execute_script("arguments[0].scrollTop = " + str(y), parent_scroll_view)
                y += 300
                time.sleep(2)
                if y >= int(scroll_height):
                    driver.execute_script("arguments[0].scrollTop = 0", parent_scroll_view)
                    time.sleep(1)
                    break
        else:
            pass
        events = driver.find_element_by_class_name('event-group').find_elements_by_class_name('grid-event-wrapper')
        market_links = []
        for event in events:
            """ start_time = event.find_element_by_class_name('grid-event-timer').text
            if "Today" not in start_time:
                continue """
            market_links.append(event.get_attribute('href'))
        for market_link in market_links:
            print(market_link)
            driver.get(market_link)
            # time.sleep(5)
            try:
                close_icon = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'span.ui-icon.theme-ex')))
                # close_icon = driver.find_element_by_css_selector('span.ui-icon.theme-ex')
                driver.execute_script("arguments[0].click();", close_icon)
                # time.sleep(5)
            except:
                pass
                
            try:
                main_view = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.ID, 'main-view')))
            except TimeoutException:
                print("Borgata: Time out waiting for page to load")
                driver.close()
                driver.quit()
                workbook.close()
                return
        
            # main_view = driver.find_element_by_id('main-view')
            event_detail_header = main_view.find_element_by_css_selector('ms-header.event-detail-header')
            tab_bar_container = event_detail_header.find_element_by_css_selector('ul.tab-bar-container')
            tab_bar_items = tab_bar_container.find_elements_by_class_name('tab-bar-item')

            player_special_exist = False
            for tab_bar_item in tab_bar_items:
                try:
                    tab_bar_text = tab_bar_item.find_element_by_css_selector('span.title').text
                except:
                    continue
                if tab_bar_text == "Player specials":
                    tab_bar_item.find_element_by_tag_name('a').click()
                    time.sleep(2)
                    player_special_exist = True
                    break
            if not player_special_exist:
                continue
            child_scroll_view = main_view.find_element_by_xpath('..')
            # scroll_height = main_view.size['height']
            scroll_height = 3000
            y = 300
            while True:
                driver.execute_script("arguments[0].scrollTop = " + str(y), child_scroll_view)
                y += 300
                time.sleep(2)
                main_view_height = main_view.size['height']
                if y > main_view_height or y > scroll_height:
                    break
            event_detail_wrapper = main_view.find_element_by_css_selector('div.event-detail-wrapper')
            option_panels = event_detail_wrapper.find_elements_by_css_selector('ms-option-panel.option-panel')
            for option_panel in option_panels:
                option_group_name = option_panel.find_element_by_css_selector('div.option-group-name').find_element_by_tag_name('div').get_attribute('innerHTML').strip()
                option_group_container = option_panel.find_element_by_class_name('option-group-container')

                try:
                    label = option_group_container.find_element_by_class_name('name').get_attribute('innerHTML').replace("Over", "").strip()
                    price1 = option_group_container.find_elements_by_class_name('value')[0].get_attribute('innerHTML').strip()
                    price2 = option_group_container.find_elements_by_class_name('value')[1].get_attribute('innerHTML').strip()
                except:
                    continue
                if "How many" in option_group_name:
                    splits = option_group_name.split(" ")
                    player_name = remove_spec(splits[4]) + " " + remove_spec(splits[5])
                    if "many points" in option_group_name:
                        write_data(points_sheet, point_row, player_name, label, price1, price2)
                        point_row += 1
                    if "many assists" in option_group_name:
                        write_data(assists_sheet, assist_row, player_name, label, price1, price2)
                        assist_row += 1
                    if "many rebounds" in option_group_name:
                        write_data(rebounds_sheet, rebound_row, player_name, label, price1, price2)
                        rebound_row += 1
                    if "many total" in option_group_name:
                        player_name = remove_spec(splits[8]) + " " + remove_spec(splits[9])
                        write_data(pra_sheet, pra_row, player_name, label, price1, price2)
                        pra_row += 1
                    if "three-pointers will" in option_group_name:
                        write_data(three_points_sheet, three_points_row, player_name, label, price1, price2)
                        three_points_row += 1
                elif "Over/Under" in option_group_name:
                    splits = option_group_name.split(" ")
                    player_name = remove_spec(splits[0]) + " " + remove_spec(splits[1])
                    if "Over/Under Points" in option_group_name:
                        write_data(points_sheet, point_row, player_name, label, price1, price2)
                        point_row += 1
                    if "Over/Under Assists" in option_group_name:
                        write_data(assists_sheet, assist_row, player_name, label, price1, price2)
                        assist_row += 1
                    if "Over/Under Rebounds" in option_group_name:
                        write_data(rebounds_sheet, rebound_row, player_name, label, price1, price2)
                        rebound_row += 1
                    if "Over/Under Three-pointers" in option_group_name:
                        write_data(three_points_sheet, three_points_row, player_name, label, price1, price2)
                        three_points_row += 1
                elif "Points + Rebounds + Assists" in option_group_name:
                    splits = option_group_name.split(" ")
                    player_name = remove_spec(splits[0]) + " " + remove_spec(splits[1])
                    write_data(pra_sheet, pra_row, player_name, label, price1, price2)
                    pra_row += 1
                elif "Will" in option_group_name and ("record a double-double" in option_group_name or "achieve a double-double" in option_group_name):
                    print(option_group_name)
                    splits = option_group_name.split(" ")
                    player_name = remove_spec(splits[1]) + " " + remove_spec(splits[2])
                    write_data(dbl_sheet, dbl_row, player_name, label, price1, price2)
                    dbl_row += 1
                    # Not Found Correct Sentence Type
                    """ if "total points" in option_group_name:
                        write_data(pra_sheet, pra_row, player_name, label, price1, price2)
                        pra_row += 1
                    if "three-pointers" in option_group_name:
                        write_data(three_points_sheet, three_points_row, player_name, label, price1, price2)
                        three_points_row += 1 """
    except:
        pass
    workbook.close()
    driver.close()
    driver.quit()
    print('---End Borgata---')

if __name__ == "__main__":
    borgata_odds()