from draftkings import draftkings_odds
from pinnacle import pinnacle_odds
from pointsbet import pointsbet_odds
from fanduel import fanduel_odds
from borgata import borgata_odds
from caesars import caesars_odds

from sportsgrid import sportsgrid_odds
from monster import monster_odds

from datetime import datetime
from threading import Thread
import xlrd
import xlsxwriter
import os
import time

filename = "output/nba_player_props_{}.xlsx".format(datetime.now().strftime('%Y%m%d%H%M'))
sheet_names = ["Rebounds", "Points", "Assists", "3-pointers", "PRA", "Dbl-Dbl"]

def get_dict(worksheet):
    dict_data = {}
    for i in range(worksheet.nrows):
        key = worksheet.cell_value(i, 0) # player name
        value = (worksheet.cell_value(i, 1), worksheet.cell_value(i, 2), worksheet.cell_value(i, 3), worksheet.cell_value(i, 4))
        dict_data[key] = value
    return dict_data

def write_data(worksheet, row, player_name, odds_value, sheet_name):
    worksheet.write(row, 0, player_name)
    idx = 0
    if sheet_name == "Dbl-Dbl":
        worksheet.write(row, 2, 'Yes')
        worksheet.write(row, 3, 'No')
        for i in range(1, 4):
            for j in range(4):
                if sheet_name == "Dbl-Dbl" and (odds_value[idx] == "0.5" or odds_value[idx] == 'Yes'):
                    worksheet.write(row + i, j, None)
                else:
                    worksheet.write(row + i, j, odds_value[idx])
                idx += 1
    else:
        worksheet.write(row, 4, odds_value[25])
        worksheet.write(row, 5, odds_value[29])
    
        for i in range(1, 7):
            for j in range(4):
                worksheet.write(row + i, j, odds_value[idx])
                idx += 1

if __name__ == "__main__":
    threads = []
    draftkings_process = Thread(target=draftkings_odds)
    draftkings_process.start()
    threads.append(draftkings_process)

    pinnacle_process = Thread(target=pinnacle_odds)
    pinnacle_process.start()
    threads.append(pinnacle_process)

    pointsbet_process = Thread(target=pointsbet_odds)
    pointsbet_process.start()
    threads.append(pointsbet_process)

    fanduel_process = Thread(target=fanduel_odds)
    fanduel_process.start()
    threads.append(fanduel_process)

    borgata_process = Thread(target=borgata_odds)
    borgata_process.start()
    threads.append(borgata_process)

    caesars_process = Thread(target=caesars_odds)
    caesars_process.start()
    threads.append(caesars_process)
    
    sportsgrid_process = Thread(target=sportsgrid_odds)
    sportsgrid_process.start()
    threads.append(sportsgrid_process)

    monster_process = Thread(target=monster_odds)
    monster_process.start()
    threads.append(monster_process)

    for process in threads:
        process.join()
    
    print('---Start Compare File Creation---')
    wb_draftkings = xlrd.open_workbook('draftkings.xlsx')
    wb_pinnacle = xlrd.open_workbook('pinnacle.xlsx')
    wb_pointsbet = xlrd.open_workbook('pointsbet.xlsx')
    wb_fanduel = xlrd.open_workbook('fanduel.xlsx')
    wb_borgata = xlrd.open_workbook('borgata.xlsx')
    wb_caesars = xlrd.open_workbook('caesars.xlsx')
    
    wb_monster = xlrd.open_workbook('monster.xlsx')
    wb_sportsgrid = xlrd.open_workbook('sportsgrid.xlsx')

    workbook = xlsxwriter.Workbook(filename)
    for sheet_name in sheet_names:
        # final excel sheet
        worksheet = workbook.add_worksheet(sheet_name)
        if sheet_name == "Dbl-Dbl":
            dk = get_dict(wb_draftkings.sheet_by_name(sheet_name))
            pn = get_dict(wb_pinnacle.sheet_by_name(sheet_name))
            bg = get_dict(wb_borgata.sheet_by_name(sheet_name))
            z = {**dk, **pn, **bg}
            
            row = 0
            for x in z:
                if x in pn:
                    z[x] = pn[x]
                else:
                    z[x] = ('PINNACLE', '', '', '')
                if x in dk:
                    z[x] = z[x] + dk[x]
                else:
                    z[x] = z[x] + ('DRAFTKINGS', '', '', '')
                if x in bg:
                    z[x] = z[x] + bg[x]
                else:
                    z[x] = z[x] + ('BORGATA', '', '', '')
                write_data(worksheet, row, x, z[x], sheet_name)
                row += 5
        else:
            # read each sheet
            dk = get_dict(wb_draftkings.sheet_by_name(sheet_name))
            pn = get_dict(wb_pinnacle.sheet_by_name(sheet_name))
            pb = get_dict(wb_pointsbet.sheet_by_name(sheet_name))
            fd = get_dict(wb_fanduel.sheet_by_name(sheet_name))
            bg = get_dict(wb_borgata.sheet_by_name(sheet_name))
            cs = get_dict(wb_caesars.sheet_by_name(sheet_name))
            bs = get_dict(wb_monster.sheet_by_name(sheet_name))
            sg = get_dict(wb_sportsgrid.sheet_by_name(sheet_name))

            z = {**dk, **pn, **pb, **fd, **bg, **cs, **bs, **sg}
            # z = {**dk, **pn, **pb, **fd, **bs, **sg}
            row = 0
            for x in z:
                if x in pn:
                    z[x] = pn[x]
                else:
                    z[x] = ('PINNACLE', '', '', '')
                if x in fd:
                    z[x] = z[x] + fd[x]
                else:
                    z[x] = z[x] + ('FANDUEL', '', '', '')
                if x in dk:
                    z[x] = z[x] + dk[x]
                else:
                    z[x] = z[x] + ('DRAFTKINGS', '', '', '')
                if x in pb:
                    z[x] = z[x] + pb[x]
                else:
                    z[x] = z[x] + ('POINTSBET', '', '', '')
                if x in bg:
                    z[x] = z[x] + bg[x]
                else:
                    z[x] = z[x] + ('BORGATA', '', '', '')
                if x in cs:
                    z[x] = z[x] + cs[x]
                else:
                    z[x] = z[x] + ('CAESARS', '', '', '')
                if x in bs:
                    z[x] = z[x] + bs[x]
                else:
                    z[x] = z[x] + ('MONSTER', '', '', '')
                if x in sg:
                    z[x] = z[x] + sg[x]
                else:
                    z[x] = z[x] + ('SPORTSGRID', '', '', '')
                write_data(worksheet, row, x, z[x], sheet_name)
                row += 8
    workbook.close()
    time.sleep(1)
    # remove the other files
    os.remove("draftkings.xlsx")
    os.remove("pinnacle.xlsx")
    os.remove("pointsbet.xlsx")
    os.remove("fanduel.xlsx")
    os.remove("borgata.xlsx")
    os.remove("caesars.xlsx")
    os.remove("monster.xlsx")
    os.remove("sportsgrid.xlsx")

    print('---End Process---')
