from bs4 import BeautifulSoup
from unidecode import unidecode
from datetime import datetime
import requests
import time
import xlsxwriter
import re

site_name = "DRAFTKINGS"
filename = 'draftkings.xlsx'
sheet_names = ["Rebounds", "Points", "Assists", "3-pointers", "PRA", "Dbl-Dbl"]
category_ids = ["1287", "1282", "1280", "1279", "1286", "1291"]

base_url = "https://sportsbook.draftkings.com/leagues/basketball/103?category=player-props&subcategory={}"

headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
    "Accept-Encoding": "gzip, deflate, br", 
    "Accept-Language": "en-US,en;q=0.9", 
    "Host": "sportsbook.draftkings.com", 
    "Sec-Fetch-Mode": "navigate", 
    "Sec-Fetch-Site": "none", 
    "Upgrade-Insecure-Requests": "1", 
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
}

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(' ')
    if len(splits) == 3:
        if len(splits[0]) == 2 and splits[0][1] == '.' and len(splits[1]) == 2 and splits[1][1] == '.':
            result = splits[0][0] + splits[1][0] + " " + splits[2]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1]
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result
    
def get_result(scrape_url, worksheet):
    page = requests.get(scrape_url, headers=headers)
    if page.status_code == 200:
        soup = BeautifulSoup(page.text, 'html.parser')
        try:
            card_container = soup.find('div', {'class': 'sportsbook-responsive-card-container__body'})
            event_accordion_wrappers = card_container.find_all('div', {'class': 'sportsbook-event-accordion__wrapper'})
            if len(event_accordion_wrappers) == 0:
                print('Currently, the player prop tab does not exist')
                return
            row = 0
            for event_accordion_wrapper in event_accordion_wrappers:
                sportsbook_date = event_accordion_wrapper.find('span', {'class': 'sportsbook-event-accordion__date'}).get_text()
                """ print(sportsbook_date)
                if "Today" not in sportsbook_date:
                    continue """
                table_body = event_accordion_wrapper.find('div', {'class': 'sportsbook-table__body'})
                contents = table_body.contents
                player_elems = contents[0].find_all('span', {'class': 'sportsbook-participant-name'})
                over_outcome_labels = contents[1].find_all('span', {'class': 'sportsbook-outcome-cell__label'})
                over_outcome_odds = contents[1].find_all('span', {'class': 'sportsbook-odds american default-color'})
                under_outcome_odds = contents[2].find_all('span', {'class': 'sportsbook-odds american default-color'})
                idx = 0
                for player_elem in player_elems:
                    player_name = remove_spec(player_elem.get_text().strip())
                    try:
                        outcome_label = over_outcome_labels[idx].get_text().strip().split(' ')[1]
                    except:
                        outcome_label = None
                    over_outcome_odd = over_outcome_odds[idx].get_text().strip()
                    under_outcome_odd = under_outcome_odds[idx].get_text().strip()

                    worksheet.write(row, 0, player_name)
                    worksheet.write(row, 1, site_name)
                    worksheet.write(row, 2, outcome_label)
                    worksheet.write(row, 3, over_outcome_odd)
                    worksheet.write(row, 4, under_outcome_odd)
                    
                    idx += 1
                    row += 1
        except:
            print('Could not fetch data: {}'.format(scrape_url))
    else:
        print('Could not fetch site content')

def draftkings_odds():
    print('---Start Draftkings---')
    workbook = xlsxwriter.Workbook(filename)
    idx = 0
    for category_id in category_ids:
        worksheet = workbook.add_worksheet(sheet_names[idx])
        print('{} Market'.format(sheet_names[idx]))
        get_result(base_url.format(category_id), worksheet)
        time.sleep(3)
        idx += 1
    workbook.close()
    print('---End Draftkings---')

if __name__ == "__main__":
    draftkings_odds()