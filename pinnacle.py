from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import randint
from unidecode import unidecode
import re
import time
import xlsxwriter
from xlrd import open_workbook
import os.path
import sys
import json

with open('config.json') as cf:
    config = json.load(cf)

user_agent = config['user_agent']
timeout = config['timeout']

site_name = "PINNACLE"
filename = 'pinnacle.xlsx'

def write_data(worksheet, row, player_name, label, price1, price2):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, label)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(" ")
    if len(splits) == 3:
        if '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1] 
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result
    
def pinnacle_odds():
    print('---Start Pinnacle---')
    workbook = xlsxwriter.Workbook(filename)

    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    three_points_sheet = workbook.add_worksheet('3-pointers')
    pra_sheet = workbook.add_worksheet('PRA')
    dbl_sheet = workbook.add_worksheet("Dbl-Dbl")

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument('--user-agent={}'.format(user_agent))
    options.add_argument('log-level=2')
    
    driver = webdriver.Chrome(options=options)

    driver.get('https://www.pinnacle.com/en/basketball/nba/matchups')

    try:
        # select american odds
        odd_format_sel = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CLASS_NAME, "icon-down-arrow")))
        driver.execute_script("arguments[0].click();", odd_format_sel)
        # odd_format_sel.click()
        
    except TimeoutException:
        print("Pinnacle: Timed out waiting for page to load. Please try again.")
        workbook.close()
        driver.close()
        driver.quit()
        
    try:
        odd_dropdown_elem = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, "//ul[contains(@class, 'style_list')]")))
        driver.execute_script("arguments[0].click();", odd_dropdown_elem.find_elements_by_tag_name('a')[1])
        
    except TimeoutException:
        driver.refresh()
        odd_dropdown_elem = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, "//ul[contains(@class, 'style_list')]")))
        driver.execute_script("arguments[0].click();", odd_dropdown_elem.find_elements_by_tag_name('a')[1])
        
    # time.sleep(3)
    
    # Today Button
    try:
        date_btn_elem = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//div[contains(@class, "style_bottomBar")]')))
        btn_today = date_btn_elem.find_elements_by_tag_name('button')[1]
        if "today" in btn_today.text.lower():
            btn_today.click()
        # time.sleep(2)
    except:
        pass
    
    try:
        WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//a[contains(@href, "nba")]')))
        nba_games = driver.find_elements_by_xpath('//a[contains(@href, "nba")]')
        nba_links = []
        for nba_game in nba_games:
            # not include matchup link
            game_link = nba_game.get_attribute('href')
            if "matchups" in game_link:
                continue
            nba_links.append(game_link)
        nba_links = list(dict.fromkeys(nba_links))
        
        if len(nba_links) == 0:
            print('Pinnacle: NBA markets do not exist.')
            driver.close()
            driver.quit()
            workbook.close()
            return
            
        points_row = 0
        assists_row = 0
        rebounds_row = 0
        three_point_row = 0
        pra_row = 0
        dbl_row = 0
        
        for nba_link in nba_links:
            print(nba_link)
            driver.get(nba_link)
            try:
                btn_player_props = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.XPATH, '//button[contains(text(), "Player Props")]')))
                driver.execute_script("arguments[0].click();", btn_player_props)
                time.sleep(3)
            except:
                continue
            
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            market_groups = soup.select('div[class*="style_marketGroup"]')
            # market_groups = driver.find_elements_by_xpath('//div[starts-with(@class, "style_marketGroup")]')
            for market_group in market_groups:
                title = market_group.select_one('div[class*="style_title"]').text
                last_idx = title.find('(')
                player_name = remove_spec(title[0:last_idx].strip())
                content = market_group.select_one('div[class*="style_content"]')
                try:
                    label = content.select_one('.label').text.split(" ")[1]
                except:
                    continue
                price1 = content.select('.price')[0].text
                price2 = content.select('.price')[1].text
                if 'Points' in title:
                    write_data(points_sheet, points_row, player_name, label, price1, price2)
                    points_row += 1
                elif 'Assists' in title:
                    write_data(assists_sheet, assists_row, player_name, label, price1, price2)
                    assists_row += 1
                elif 'Rebounds' in title:
                    write_data(rebounds_sheet, rebounds_row, player_name, label, price1, price2)
                    rebounds_row += 1
                elif '3 Point FG' in title:
                    write_data(three_points_sheet, three_point_row, player_name, label, price1, price2)
                    three_point_row += 1
                elif 'Pts+Rebs+Asts' in title:
                    write_data(pra_sheet, pra_row, player_name, label, price1, price2)
                    pra_row += 1
                elif "Double+Double" in title:
                    write_data(dbl_sheet, dbl_row, player_name, label, price1, price2)
                    dbl_row += 1
                else:
                    continue
                time.sleep(1)
    except:
        pass
    
    workbook.close()
    driver.close()
    driver.quit()
    print('---End Pinnacle---')

if __name__ == "__main__":
    pinnacle_odds()