from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from random import randint
from unidecode import unidecode
import xlsxwriter
import re
import time
import json

with open('config.json') as cf:
    config = json.load(cf)

user_agent = config['user_agent']
timeout = config['timeout']

filename = 'caesars.xlsx'
site_name = "CAESARS"

def write_data(worksheet, row, player_name, label, price1, price2):
    worksheet.write(row, 0, player_name)
    worksheet.write(row, 1, site_name)
    worksheet.write(row, 2, label)
    worksheet.write(row, 3, price1)
    worksheet.write(row, 4, price2)

def remove_spec(player_name):
    unicode_str = unidecode(player_name)
    splits = unicode_str.split(" ")
    if len(splits) == 3:
        if '.' in splits[2]:
            result = splits[0] + " " + splits[1]
        elif '.' in splits[1]:
            result = splits[0] + " " + splits[2]
        elif len(splits[2]) == 2:
            result = splits[0] + " " + splits[1] 
        else:
            result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    else:
        result = re.sub('[^A-Za-z0-9 ]+', '', unicode_str)
    return result

def caesars_odds():
    print('---Start Caesars---')
    workbook = xlsxwriter.Workbook(filename)

    rebounds_sheet = workbook.add_worksheet('Rebounds')
    points_sheet = workbook.add_worksheet('Points')
    assists_sheet = workbook.add_worksheet('Assists')
    workbook.add_worksheet('3-pointers')
    workbook.add_worksheet('PRA')
    workbook.add_worksheet('Dbl-Dbl')
    
    points_row = 0
    rebounds_row = 0
    assists_row = 0

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument("start-maximized")
    options.add_argument("--headless")
    options.add_argument('--user-agent={}'.format(user_agent))
    options.add_argument('log-level=2')

    driver = webdriver.Chrome(options=options)

    driver.get('https://www.caesarscasino.com/sports/sports/competition/77/nba/matches')
    try:
        view_all_button = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'a.content-loader__load-more-link')))
        driver.execute_script("arguments[0].click();", view_all_button)
        time.sleep(2)
    except:
        pass
    
    try:
        today_markets = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div.heading.heading--timeband.heading--timeband--today')))
    except TimeoutException:
        print('Caesars: Today, there is no nba market')
        driver.close()
        driver.quit()
        workbook.close()
        return
    
    try:
        market_list = today_markets.find_element_by_class_name('event-list__content').find_elements_by_class_name('event-list__item')
        market_links = []
        for market in market_list:
            market_link = market.find_element_by_class_name('event-list__item-link-anchor').get_attribute('href')
            market_links.append(market_link)
            
        for market_link in market_links:
            print(market_link)
            driver.get(market_link)
            try:
                event_markets_container = WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div.event-markets--non-goal-scorer')))
            except TimeoutException:
                print('{}: there is no data'.format(market_link))
                continue
            event_panels = event_markets_container.find_elements_by_class_name('event-panel--market-hl')
            for event_panel in event_panels:
                title = event_panel.find_element_by_class_name('event-panel__heading__market-name').get_attribute('innerHTML')
                if title == "Total Points" or title == "1H Total Points":
                    continue
                if "Total Points" in title or "Total Rebounds" in title or "Total Assists" in title:
                    splits = title.split(" ")
                    player_name = remove_spec(splits[0]) + " " + remove_spec(splits[1])
                    expanding_heading = event_panel.find_element_by_class_name('event-panel__heading')
                    driver.execute_script("arguments[0].click();", expanding_heading)
                    time.sleep(1)
                    try:
                        expansion_body = event_panel.find_element_by_class_name('event-panel__body')
                    except:
                        continue
                    
                    try:
                        label = expansion_body.find_element_by_css_selector('span.button--outcome__text-handicapValue').get_attribute('innerHTML')
                        price1 = expansion_body.find_elements_by_css_selector('span.button--outcome__price')[0].get_attribute('innerHTML')
                        price2 = expansion_body.find_elements_by_css_selector('span.button--outcome__price')[1].get_attribute('innerHTML')
                    except:
                        continue
                    
                    if "Total Points" in title:
                        write_data(points_sheet, points_row, player_name, label, price1, price2)
                        points_row += 1
                    elif "Total Rebounds" in title:
                        write_data(rebounds_sheet, rebounds_row, player_name, label, price1, price2)
                        rebounds_row += 1
                    else:
                        write_data(assists_sheet, assists_row, player_name, label, price1, price2)
                        assists_row += 1
                    driver.execute_script("arguments[0].click();", expanding_heading)
                    time.sleep(0.5)
            time.sleep(2)
    except:
        pass
    
    workbook.close()
    driver.close()
    driver.quit()
    print('---End Caesars---')

if __name__ == "__main__":
    caesars_odds()